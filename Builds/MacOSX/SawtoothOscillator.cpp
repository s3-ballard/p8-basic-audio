//
//  SawtoothOscillator.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 11/11/2016.
//
//

#include "SawtoothOscillator.hpp"

SawWave::SawWave()
{
    
}

SawWave::~SawWave()
{
    
}

float SawWave::renderWaveShape(const float currentPhase)
{
    float out;
    
    out = 1 - ((1/M_PI) * currentPhase);
    return out;
    
}
