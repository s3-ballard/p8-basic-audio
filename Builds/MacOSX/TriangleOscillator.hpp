//
//  TriangleOscillator.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 11/11/2016.
//
//

#ifndef TriangleOscillator_hpp
#define TriangleOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

class TriWave : public Oscillator
{
    
public:
    TriWave();
    
    ~TriWave();
    
    float renderWaveShape (const float currentPhase) override;
    
    
    
    
};

#endif /* TriangleOscillator_hpp */
