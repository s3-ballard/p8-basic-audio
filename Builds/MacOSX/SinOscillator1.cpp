//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 02/11/2016.
//
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    amplitude = 0.5;
    frequency = 440;
    phasePosition = 0.0;
}

SinOscillator::~SinOscillator()
{
    
}
//change get sample and create a new function for sample rate

float SinOscillator::getSample(float sampRate)
{
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * frequency) / sampRate;
    phasePosition += phaseIncrement;
    
    if (phasePosition > twoPi)
    {
        phasePosition -= twoPi;
    }
    return (sin(phasePosition) * amplitude);
}

void SinOscillator::setFrequency(float freq)
{
    frequency = freq;
}

void SinOscillator::setAmplitude(float amp)
{
    amplitude = amp;
}
