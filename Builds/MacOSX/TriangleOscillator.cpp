//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 11/11/2016.
//
//

#include "TriangleOscillator.hpp"

TriWave::TriWave()
{
    
}

TriWave::~TriWave()
{
    
}

float TriWave::renderWaveShape (const float currentPhase)
{
    float out;
    
    if (currentPhase < M_PI)
    {
        out = 1 + ((2 / M_PI) * currentPhase);
    }
    
    else
    {
        out = 3 - ((2 / M_PI) * currentPhase);
    }
    
    return out;
}
