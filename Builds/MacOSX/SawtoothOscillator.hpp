//
//  SawtoothOscillator.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 11/11/2016.
//
//

#ifndef SawtoothOscillator_hpp
#define SawtoothOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

class SawWave : public Oscillator
{
public:
    SawWave();
    
    ~SawWave();
    
    
    float renderWaveShape (const float currentPhase) override;
    
    
    
    
};

#endif /* SawtoothOscillator_hpp */
