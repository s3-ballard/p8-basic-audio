//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 02/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class SinOscillator :public Component

{
    
public:
    SinOscillator();
    ~SinOscillator();
    
    float getSample(float sampRate);
    void setFrequency(float freq);
    void setAmplitude (float amp);
    
private:
    float phasePosition;
    float frequency;
    float amplitude;
};


#endif /* SinOscillator_hpp */
