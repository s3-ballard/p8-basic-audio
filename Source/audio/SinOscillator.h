/*
 *  SinOscillator.h
 *  sdaAudioMidi
 *
 *  Created by tjmitche on 11/11/2010.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/**
 Class for a sinewave oscillator
 */


class SinOscillator  : public Oscillator
{
public:
	//==============================================================================
	/**
	 SinOscillator constructor 
     * @see ~SinOscillator()
	 */
	SinOscillator();
    
	
	/**
	 SinOscillator destructor
     * @see SinOscillator()
	 */
	~SinOscillator();
	
	/**
	 function that provides a sine wave
     * @param takes in the phase position to produce a sine
     * @return returns sample of chosen waveform
	 */
	float renderWaveShape (const float currentPhase) override;
	
private:

};

#endif //H_SINOSCILLATOR
