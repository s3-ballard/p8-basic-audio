/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    amp = 0.0;
    osc = &sine;
    oscSelect = 0;
    
    //audioDeviceManager.setMidiInputEnabled("MicroBrute", true);
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs

    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here

    if (message.isNoteOnOrOff())
    {
        frequency = message.getMidiNoteInHertz(message.getNoteNumber());
        amp = message.getFloatVelocity();
        
    }
    
    DBG("Midi Recieved");
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    if (oscSelect.get() == 0)
    {
        osc = &sine;
    }
    
    else if (oscSelect.get() == 1)
    {
        osc = &square;
    }
    
    else if (oscSelect.get() ==2)
    {
        osc = &saw;
    }
    
    else if (oscSelect.get() == 3)
    {
        osc = &tri;
    }

    osc->setFrequency(frequency.get());
    //square.setFrequency(frequency.get());
    
    while(numSamples--)
    {
        //*outL = (*inL * amp);
        //*outR = (*inR * amp);
        
        
   
       osc->setAmplitude(amp.get());
       // square.setAmplitude(amp.get());
        
        *outL = osc->nextSample();
        *outR = osc->nextSample();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    frequency = 440.f;
    sampleRate = device->getCurrentSampleRate();
    osc->setSampleRate(sampleRate);
    //square.setSampleRate(sampleRate);
}

void Audio::audioDeviceStopped()
{

}

void Audio::Beep()
{
    //frequency = 440.f;
    //uint32 timing =  Time::getMillisecondCounter();
    
      //       amp = 1.0;
      //       Time::waitForMillisecondCounter(timing + 100);
      //       amp = 0.0;

    
}

