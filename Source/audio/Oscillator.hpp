//
//  Oscillator.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 10/11/2016.
//
//

#ifndef Oscillator_hpp
#define Oscillator_hpp

#include <stdio.h>



#include "../JuceLibraryCode/JuceHeader.h"

/**
 Class for a sinewave oscillator
 */

class Oscillator
{
public:
    //==============================================================================
    /**
     Oscillator constructor
     * @see ~Oscillator()
     */
    Oscillator();
    
    /**
     Oscillator destructor
     * @see Oscillator()
     */
    virtual ~Oscillator();
    
    /**
     sets the frequency of the oscillator
     * @see setNote()
     * @param input of a frequency as a float
     */
    void setFrequency (float freq);
    
    /**
     sets frequency using a midi note number
     * @see setFrequency()
     * @param input of a midi note number to turn into frequency
     */
    void setNote (int noteNum);
    
    /**
     sets the amplitude of the oscillator
     * @param input of amplitude (has to be between 0 and 1)
     */
    void setAmplitude (float amp);
    
    /**
     resets the oscillator
     */
    void reset();
    
    /**
     sets the sample rate
     * @param sample rate input as a float
     */
    void setSampleRate (float sr);
    
    /**
     Returns the next sample
     * @return the next sample given as a float
     */
    float nextSample();
    
    /**
     function that provides the execution of the waveshape (over-ridable for other classes)
     * @param the current phase position input
     * @return returns the sample for the waveshape
     */
    virtual float renderWaveShape (const float currentPhase);
    
private:
    float frequency;
    float amplitude;
    float sampleRate;
    float phase;
    float phaseInc;
};

#endif /* Oscillator_hpp */
