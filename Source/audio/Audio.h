/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.h"
#include "SawtoothOscillator.hpp"
#include "TriangleOscillator.hpp"
#include "Oscillator.hpp"


class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /** interpruts incoming MIDI message */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** assigns and processes audio inputs and outputs */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    /** used to initialise various parameters before audio commences */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** function called when audio device stops */
    void audioDeviceStopped() override;
    void Beep();
    
    
    Atomic<float> amp;
    Oscillator *osc;
     SquareWave square;
    Atomic<int> oscSelect;
    
private:
    AudioDeviceManager audioDeviceManager;
    Atomic<float> frequency;
    float phasePosition;
    float sampleRate;
    SinOscillator sine;
    SawWave saw;
    TriWave tri;
    
    
};



#endif  // AUDIO_H_INCLUDED
