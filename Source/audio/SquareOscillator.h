//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 10/11/2016.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR

#include <stdio.h>
#include "Oscillator.hpp"

/** Class for square wave Oscillator */

class SquareWave : public Oscillator
{ public:
    
    /** Square wave contructor 
     * @see ~SquareWave()
     */
    SquareWave();
    
    /** Square Wave Destructor
     * @see SquareWave()
     */
    ~SquareWave();
    
    /** function that allows for rendering a square wave
     * @param takes current phase position and creates a Square Wave
     * @return returns a float of the sample amplitude
     */
    float renderWaveShape (const float currentPhase) override;
    
    Atomic <float> dutyCycle;
private:
    
    
    
    
    
};




#endif /* H_SQUAREOSCILLATOR */
