//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Scott Ballard on 10/11/2016.
//
//

#include "SquareOscillator.h"

SquareWave::SquareWave()
{
    dutyCycle = M_PI;
}

SquareWave::~SquareWave()
{
    
}

float SquareWave::renderWaveShape (const float currentPhase)
{
    float wave;
    if (currentPhase <= dutyCycle.get())
    {
        wave = 1;
    }
    else if (currentPhase > dutyCycle.get() && currentPhase <= 2 * M_PI)
    {
        wave = -1;
    }
    
    return wave;
}