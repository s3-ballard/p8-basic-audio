/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "Counter.hpp"
#include "SquareOscillator.h"



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
/** class for the main component / UI of the application */
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Slider::Listener,
public Button::Listener,
public Counter::Listener, public ComboBox::Listener



{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& a);

    /** Destructor */
    ~MainComponent();

    /** function called when window has been resized */
    void resized() override;

    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    /** function called whenever a slider has changed value */
    void sliderValueChanged(Slider* slider) override;
    /** function called whenever a button is clicked */
    void buttonClicked (Button* button) override;
    
    void counterChanged (const unsigned int counterValue) override;
    /** function called whenever a combo box has changed - used to change waveform */
    void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
    
    
private:
    Audio& audio;
    Slider ampSlider;
    TextButton button1;
    //int counter;
    Counter count;
    Slider tempoSlider;
    ComboBox oscSel;
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
