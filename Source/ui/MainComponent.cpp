/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) :  audio (a)
{

    setSize (500, 400);
    addAndMakeVisible(button1);
    ampSlider.setSliderStyle(Slider::LinearHorizontal);
    ampSlider.setRange(0, 1);
    button1.setButtonText("press to stop / start");
    button1.setBounds(10, 90, getWidth()-20, 40);
    button1.addListener(this);
    
    addAndMakeVisible(tempoSlider);
    tempoSlider.setSliderStyle(Slider::LinearHorizontal);
    tempoSlider.setRange(0.01, 2*M_PI);
    tempoSlider.setBounds(10, 150, getWidth()-20, 40);
    tempoSlider.addListener(this);
    
    addAndMakeVisible(count);
    count.setListener(this);
    //count.run();
    
    addAndMakeVisible(oscSel);
    oscSel.addItem("Sine", 1);
    oscSel.addItem("Square", 2);
    oscSel.addItem("Saw", 3);
    oscSel.addItem("Tri", 4);
    oscSel.setBounds(10, 200, getWidth()-20, 40);
    oscSel.addListener(this);
    
    

}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    
    if (slider == &ampSlider)
    {
       // audio.amp = ampSlider.getValue();
    }
    
    if (slider == &tempoSlider)
    {
        count.countTime = tempoSlider.getValue();
        audio.square.dutyCycle = tempoSlider.getValue();
        
    }
    
}

void MainComponent::buttonClicked(Button* button)
{
    count.stopOrStartCounter();
    
}


void MainComponent::counterChanged(const unsigned int counterValue)
{
   // std::cout << "Counter:" << counterValue << "\n";
    //audio.Beep();
    
}

void MainComponent::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &oscSel)
    {
        if (oscSel.getSelectedId() == 1) {
            audio.oscSelect = 0;
            
        }
        
        else if (oscSel.getSelectedId() == 2)
        {
            audio.oscSelect = 1;
        }
        
        else if (oscSel.getSelectedId() == 3)
        {
            audio.oscSelect = 2;
        }
       
        else if (oscSel.getSelectedId() == 4)
        {
            audio.oscSelect = 3;
        }
    }
}
